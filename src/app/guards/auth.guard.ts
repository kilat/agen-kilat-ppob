import { Injectable } from '@angular/core';
import {CanLoad, Route, Router, UrlSegment, UrlTree} from '@angular/router';
import { Observable } from 'rxjs';
import {AuthenticationService} from '../services/authentication/authentication.service';
import {filter, map, take} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private authService: AuthenticationService, private router: Router) { }

  // canLoad(
  //   route: Route,
  //   segments: UrlSegment[]): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
  //   return true;
  // }
  // @ts-ignore
  canLoad(): Observable<boolean | null> {
    return this.authService.isAuthenticated.pipe(
      filter(val => val !== null), // Filter out initial Behaviour subject value
      take(1), // Otherwise the Observable doesn't complete!
      map(isAuthenticated => {
        // console.log('cek isAuthenticated', isAuthenticated)
        if (isAuthenticated) {
          return true;
        } else {
          this.router.navigateByUrl('/login').then();
          return false;
        }
      })
    );

    // setTimeout(() => {
    //   return this.authService.isAuthenticated.pipe(
    //     filter(val => val !== null), // Filter out initial Behaviour subject value
    //     take(1), // Otherwise the Observable doesn't complete!
    //     map(isAuthenticated => {
    //       // console.log('cek isAuthenticated', isAuthenticated)
    //       if (isAuthenticated) {
    //         return true;
    //       } else {
    //         this.router.navigateByUrl('/login').then();
    //         return false;
    //       }
    //     })
    //   );
    // }, 500);
    // return this.authService.isAuthenticated.pipe(
    //   filter(val => val !== null), // Filter out initial Behaviour subject value
    //   take(1), // Otherwise the Observable doesn't complete!
    //   map(isAuthenticated => {
    //     alert('aduh')
    //     if (isAuthenticated) {
    //       return true;
    //     } else {
    //       this.router.navigateByUrl('/login').then();
    //       return false;
    //     }
    //   })
    // );
  }
}
