import { Component, Input, OnInit } from '@angular/core';
import { DepositPage } from '../../pages/deposit/deposit.page';

@Component({
  selector: 'app-deposit-auto',
  templateUrl: './deposit-auto.component.html',
  styleUrls: ['./deposit-auto.component.scss'],
})
export class DepositAutoComponent implements OnInit {

  // vaList: { label: string; css: string, id: number }[];
  @Input() vaList: any;
  @Input() selectedBank: string = '';

  constructor(public depositPage: DepositPage) {
    // this.vaList = [
    //   { id: 0, label: 'Virtual Account BCA', css: 'nanti' },
    //   { id: 1, label: 'Virtual Account Mandiri', css: 'nanti' },
    //   { id: 2, label: 'Virtual Account BRI', css: 'nanti' },
    //   { id: 3, label: 'Virtual Account BNI', css: 'nanti' },
    //   { id: 4, label: 'Virtual Account Permata', css: 'nanti' },
    // ];
  }

  ngOnInit() {}

  listClicked(data: any) {
    this.selectedBank = '';
    this.vaList.forEach((res: any, i: number) => {
      if (i === data.id) {
        res.css = 'activeList'
        this.selectedBank = data.code;
        this.depositPage.selectedBank = data.code;
      } else {
        res.css = 'nanti'
      }
    })
  }

}
