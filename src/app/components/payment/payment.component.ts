import { Component, OnInit } from '@angular/core';
import {PaymentPage} from "../../pages/payment/payment.page";

@Component({
  selector: 'app-payment-component',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {

  constructor(
    public paymentPage: PaymentPage
  ) { }

  ngOnInit() {
    // console.log('this.paymentPage', this.paymentPage)
  }

}
