import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {ActionSheetController, Platform} from '@ionic/angular';
import {Camera, CameraOptions} from '@awesome-cordova-plugins/camera/ngx';
import {RequestDepositModel} from '../../models/users.model';
import {UserService} from '../../services/user/user.service';
import {LoadingService} from '../../services/loading/loading.service';
import {AlertService} from '../../services/alert/alert.service';
import {HelpersService} from "../../services/helpers/helpers.service";

@Component({
  selector: 'app-deposit',
  templateUrl: './deposit.page.html',
  styleUrls: ['./deposit.page.scss'],
})
export class DepositPage implements OnInit {

  img: any;
  imgSave: string = '';
  requestDeposit: RequestDepositModel = new RequestDepositModel();
  segment: string = '';
  amountStr: string = '';
  vaList: { label: string; css: string, id: number, code: string }[] = [];
  selectedBank: string = '';

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    public actionSheetController: ActionSheetController,
    private camera: Camera,
    private plt: Platform,
    private userService: UserService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private helpersService: HelpersService,
  ) {
    // this.plt.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  ngOnInit() {
    // console.log(this.selectedBank);
  }

  async ionViewWillEnter() {
    this.segment = 'manual';
    this.amountStr = '0';
    this.imgSave = '';
    this.requestDeposit = new RequestDepositModel();
    this.vaList = [
      { id: 0, label: 'Virtual Account BCA', css: 'nanti', code: 'CENA' },
      { id: 1, label: 'Virtual Account Mandiri', css: 'nanti', code: 'BMRI' },
      { id: 2, label: 'Virtual Account BRI', css: 'nanti', code: 'BRIN' },
      { id: 3, label: 'Virtual Account BNI', css: 'nanti', code: 'BNIN' },
      { id: 4, label: 'Virtual Account Permata', css: 'nanti', code: 'BBBA' },
    ];
    this.selectedBank = '';
  }

  async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Image source',
      cssClass: 'my-custom-class',
      buttons: [{
        text: 'Load from Library',
        handler: () => {
          console.log('Load from Library');
          this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
        }
      }, {
        text: 'Use Camera',
        // icon: 'share',
        handler: () => {
          console.log('Use Camera clicked');
          this.showCamera();
        }
      },
        {
          text: 'Cancel',
          icon: 'close',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }]
    });
    await actionSheet.present();
  }


  showCamera() {
    this.imgSave = '';
    const options: CameraOptions = {
      quality: 50,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
    };
    this.camera.getPicture(options).then((imageData) => {
      alert(imageData);
      this.imgSave = 'data:image/jpeg;base64,' + imageData;
      // this.img = this.webview.convertFileSrc(imageData)
    }, (err) => {
      alert(JSON.stringify(err));
    });
  }

  takePicture(sourceType: any) {
    this.imgSave = '';
    // tslint:disable-next-line:prefer-const
    const options: CameraOptions = {
      quality: 50,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true,
      destinationType: this.camera.DestinationType.DATA_URL,
    };

    this.camera.getPicture(options).then(imagePath => {
      if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        const base64Image = 'data:image/jpeg;base64,' + imagePath;
        this.imgSave = base64Image;
      } else {
        const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        // this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
      }
    });

  }

  async saveDeposit() {
    await this.loadingService.present();
    this.requestDeposit.amount = Number(this.amountStr.replace(',', '').replace(',', '').replace(',', ''));
    this.requestDeposit.img = this.imgSave;
    try {
      const response = await this.userService.saveDepositUser(this.requestDeposit);
      await this.loadingService.dismiss();
      if (response.message === 'created') {
        await this.alertService.presentAlert('Success', 'Request Deposit Success');
        await this.ionViewWillEnter();
        return;
      }
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e.error.message);
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async saveDepositAuto() {
    if (!this.selectedBank) {
      await this.alertService.presentAlert('Error', 'Bank harus di pilih');
      return;
    }
    const amount = Number(this.amountStr.replace(',', '').replace(',', '').replace(',', ''));
    const params = JSON.stringify({bill_id: '081219836581', product_code: this.selectedBank, category: 'VA001', amount});
    await this.router.navigate(['/tabs/home/payment', {data: params}]);
  }

  async segmentChanged(e: any) {
    console.log(e?.detail?.value)
  }

  async amountInput(event: any) {
    let tmpVal = Number(event.target.value.replace(',', '').replace(',', '').replace(',', ''));
    this.amountStr = this.helpersService.billIdFormat(tmpVal.toString());
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
