import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DepositPageRoutingModule } from './deposit-routing.module';

import { DepositPage } from './deposit.page';
import {DepositAutoComponent} from "../../components/deposit-auto/deposit-auto.component";
import {DepositManualComponent} from "../../components/deposit-manual/deposit-manual.component";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DepositPageRoutingModule
  ],
  declarations: [DepositPage, DepositAutoComponent, DepositManualComponent]
})
export class DepositPageModule {}
