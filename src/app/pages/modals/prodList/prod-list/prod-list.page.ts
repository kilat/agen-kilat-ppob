import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { ProductsModel } from "../../../../models/products.model";
import { LoadingService } from "../../../../services/loading/loading.service";
import { AlertService } from "../../../../services/alert/alert.service";

@Component({
  selector: 'app-prod-list',
  templateUrl: './prod-list.page.html',
  styleUrls: ['./prod-list.page.scss'],
})
export class ProdListPage implements OnInit {

  @Input() products: ProductsModel[] = [];
  // productFileter: { name_cat: string; category: string }[] = [];
  // productFileterTemp: { name_cat: string; category: string }[] = [];
  productFileter: { brand: string; }[] = [];
  productFileterTemp: { brand: string; }[] = [];
  prefix: string = '';

  constructor(
    private loadingService: LoadingService,
    private alertService: AlertService,
    private modal: ModalController
  ) { }

  async ngOnInit() {
    await this.getProductData();
  }

  async getProductData() {
    await this.loadingService.present();
    try {
      const checkProdName: any[] = [];
      this.products.forEach((val: any) => {
        if (!checkProdName.includes(val.brand)) {
          console.log(val.brand);
          checkProdName.push(val.brand);
          this.productFileterTemp.push({
            // name_cat: val.name_cat || '',
            // category: val.category.name || '',
            brand: val.brand || '',
          });
          this.productFileter = this.productFileterTemp;
        }
      });
      this.productFileter.sort((a, b) => a.brand.localeCompare(b.brand));
      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e?.error?.message || 'Internal server error');
    }
  }

  async getProdItem(prod: any){
    await this.modal.dismiss({
      dismissed: true,
      data: prod
    });
  }

  async filterList(e: any) {
    const searchTerm = e.srcElement.value;
    this.productFileter = this.productFileterTemp;

    if (!searchTerm) {
      return;
    }

    // @ts-ignore
    this.productFileter = this.productFileter.filter(currentList => {
      if (currentList.brand && searchTerm) {
        return (currentList.brand.toLowerCase().indexOf(searchTerm.toLowerCase()) > -1);
      }
    });
  }

  async dismiss() {
    await this.modal.dismiss({
      dismissed: true,
      data: null
    });
  }

}
