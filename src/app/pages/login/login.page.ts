import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators, FormControl} from '@angular/forms';
import {AlertController, Platform} from '@ionic/angular';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {LoadingService} from '../../services/loading/loading.service';
import {AlertService} from '../../services/alert/alert.service';
import { GoogleAuth } from '@codetrix-studio/capacitor-google-auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  credentials: FormGroup = new FormGroup({
    email: new FormControl(),
    password: new FormControl('123'),
  });
  user: any;

  constructor(private fb: FormBuilder,
              private authService: AuthenticationService,
              private alertController: AlertController,
              private router: Router,
              private loadingService: LoadingService,
              private alertService: AlertService,
              private plt: Platform,
  ) {
    // this.plt.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  // ionViewWillEnter(){
  //   alert(environment.apiUrl);
  // }

  ngOnInit() {
    if (!this.plt.is('capacitor')) {
      GoogleAuth.initialize();
    }
    this.credentials = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
    });
  }

  async login() {
    await this.loadingService.present();

    if (!this.credentials) {
      await this.alertService.presentAlert('Login failed', 'credential undefined');
      await this.loadingService.dismiss();
      return;
    }

    this.authService.login(this.credentials.value).subscribe(
      async (_) => {
        // console.log(res);
        const isAuthenticated = await this.authService.isAuthenticated;
        // console.log(isAuthenticated.getValue());
        await this.loadingService.dismiss();
        if (isAuthenticated.getValue()) {
          await this.router.navigateByUrl('tabs/home', {replaceUrl: true});
        } else {
          await this.alertService.presentAlert('Login failed', 'Role anda bukan loket');
        }
      },
      async (res) => {
        // console.log(res);
        await this.loadingService.dismiss();
        await this.alertService.presentAlert('Login failed', res.error.message);
      }
    );

  }

  // Easy access for form fields
  // eslint-disable-next-line @typescript-eslint/member-ordering
  get email() {
    if (!this.credentials) {
      this.alertService.presentAlert('Login failed', 'credential undefined').then();
      return;
    }
    return this.credentials.get('email');
  }

  // eslint-disable-next-line @typescript-eslint/member-ordering
  get password() {
    if (!this.credentials) {
      this.alertService.presentAlert('Login failed', 'credential undefined').then();
      return;
    }
    return this.credentials.get('password');
  }

  async loginByGoogle() {
    await this.loadingService.present();
    const gAuth = await GoogleAuth.signIn();

    if (gAuth.email) {
      this.authService.loginByGoogle(gAuth).subscribe(
        async (_) => {
          // console.log(res);
          setTimeout(async () => {
            const isAuthenticated = await this.authService.isAuthenticated;
            await this.loadingService.dismiss();
            if (isAuthenticated.getValue()) {
              await this.router.navigateByUrl('/tabs/home', {replaceUrl: true});
            } else {
              await this.alertService.presentAlert('Login failed', 'Role anda bukan loket');
            }
          }, 1000);
        },
        async (res) => {
          // console.log(res);
          await this.loadingService.dismiss();
          await this.alertService.presentAlert('Login failed', res.error.message);
        }
      );
    }

    await this.loadingService.dismiss();
  }
}
