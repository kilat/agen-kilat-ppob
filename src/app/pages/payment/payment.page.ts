import {Component, OnInit} from '@angular/core';
import {InqReqModel, InqResPulsaModel, PayReqModel, PayResPulsaModel, TrxDataModel} from "../../models/trx.model";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {LoadingService} from "../../services/loading/loading.service";
import {AlertService} from "../../services/alert/alert.service";
import {TrxService} from "../../services/trx/trx.service";
import {formatNumber} from "@angular/common";
import {UserService} from "../../services/user/user.service";
import {UserDataModel} from "../../models/users.model";
import {Storage} from "@capacitor/storage";
import {PrintSettingService} from "../../services/print-setting/print-setting.service";
// import {Platform} from "@ionic/angular";

@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  posibleNull: string = '';
  user: UserDataModel = new UserDataModel();
  trxData: TrxDataModel = new TrxDataModel();
  inqData: InqReqModel = new InqReqModel();
  inqRes: InqResPulsaModel = new InqResPulsaModel();
  payData: PayReqModel = new PayReqModel();
  payRes: PayResPulsaModel = new PayResPulsaModel();
  paramsData: { bill_id: string; product_code: string, category: string, amount: number }
    = {bill_id: '', product_code: '', category: '', amount: 0}
  statePage: string = 'inq';
  timeStamp_str: string = '';
  amount_str: string = '';
  admin_str: string = '';
  total_str: string = '';
  printerMacAddress: string = 'printerMcAddress';

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private trxService: TrxService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private userService: UserService,
    private print: PrintSettingService,
    // private plt: Platform,
  ) {
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.paramsData = JSON.parse(<string>this.route.snapshot.paramMap.get('data'));
    await this.reqInq();
    this.user = await this.getUser();
  }

  async reqInq() {
    // this.products = [];
    await this.loadingService.present();
    try {
      if (this.paramsData.bill_id) {
        this.paramsData.bill_id = this.paramsData.bill_id.split('-').join('');
      }
      this.inqRes = await this.trxService.reqInq(this.paramsData);

      if (this.inqRes.result_cd !== '00') {
        this.statePage = 'fail';
        await this.loadingService.dismiss();
        await this.alertService.presentAlert('Error', this.inqRes.result_msg);
        return
      }

      const {year, month, day, hours, minutes} = await this.trxService.getDate();
      this.timeStamp_str = this.trxService.pad2(day) + '-' + this.trxService.pad2(month) + '-' +
        year + ' ' + hours + ':' + minutes;

      this.amount_str = formatNumber(this.inqRes.amount, 'en-US', '1.0');
      this.admin_str = formatNumber(this.inqRes.admin, 'en-US', '1.0');
      this.total_str = formatNumber(this.inqRes.total_amount, 'en-US', '1.0');

      this.payData.bill_id = this.inqRes.bill_id;
      this.payData.user_id = this.inqRes.user_id;
      this.payData.ref_id = this.inqRes.ref_id;
      this.payData.timeStamp = this.inqRes.timeStamp;
      this.payData.appName = this.inqRes.appName;
      this.payData.product_code = this.inqRes.product_code;
      this.payData.merchantToken = this.inqRes.merchantToken;
      this.payData.tx_id = this.inqRes.tx_id;
      this.payData.amount = this.inqRes.amount;
      this.payData.admin = this.inqRes.admin;
      this.payData.total_amount = this.inqRes.total_amount;
      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      // console.log(e)
      await this.alertService.presentAlert('Error', e.error ? e.error.message : 'server error');
      // console.log('error is : ' + e);
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async reqPay() {
    await this.loadingService.present();
    try {
      const response = await this.trxService.reqPay(this.payData);
      await this.loadingService.dismiss();
      if (response.result_cd === '00' || response.result_cd === '01' || response.result_cd === '5') {
        this.statePage = 'pay';
        this.payRes = response;
        if (this.paramsData?.product_code === 'PLNPOST001' ||
          this.paramsData?.category === 'PDAM001' ||
          this.paramsData?.category === 'BPJS001') {
          await this.alertService.presentAlert('Success', 'Transaksi sekses');
        } else {
          await this.alertService.presentAlert('Success', 'Transaksi sekses, dengan status pending');
        }
        return;
      }
      this.statePage = 'fail';
      await this.alertService.presentAlert('Failed', 'Transaksi Gagal');
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e.error ? e.error.message : 'server error');
      console.log('error is : ' + e);
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async getUser() {
    const response = await this.userService.getUserData();
    return await response.data;
  }

  async backToHome() {
    await this.router.navigateByUrl('/tabs/home', {replaceUrl: true});
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

  async printRequest() {
    if (this.paramsData?.category === 'PDAM001') {
      await this.printSuccessPDAM();
    }
  }

  async printSuccessPDAM() {
    let myText = 'Kilat Telknologi Mandiri \n';
    myText += this.payData.timeStamp + ' \n';
    myText += 'Loket : ' + this.user.full_name + ' \n\n\n';

    myText += 'TRX Id : ' + this.payData.tx_id + '\n';
    myText += 'PDAM : ' + this.payRes?.detail?.product_name || 'PDAM' + '\n';
    myText += 'Id Pelanggan : ' + this.payRes.bill_id + ' \n';
    myText += 'Nama : ' + this.inqRes.detail.cust_name + '\n';
    myText += 'Tanggal : ' + this.payRes.timeStamp + ' \n';
    myText += 'Jml Bln : ' + this.inqRes.detail.period + ' \n';
    myText += 'Tot Tagihan : ' + formatNumber(Number(this.inqRes.amount), 'en-US', '1.0-0') + ' \n';
    myText += 'Tot Admin : ' + formatNumber(Number(this.inqRes.admin), 'en-US', '1.0-0') + ' \n';
    myText += 'Total : ' + formatNumber(Number(this.inqRes.total_amount), 'en-US', '1.0-0') + ' \n\n';

    myText += 'Menyatakan Struk Ini Adalah Bukti Pembayaran Yang Sah \n\n';

    myText += 'Terimakasih \n';
    myText += 'Kilat Teknologi Mandiri \n\n\n\n\n';

    console.log(myText);

    const printer = await Storage.get({ key: this.printerMacAddress });
    if (printer && printer.value) {
      // console.log('set token: ', token.value);
      this.print.sendToBluetoothPrinter(printer.value, myText);
    }
  }

}
