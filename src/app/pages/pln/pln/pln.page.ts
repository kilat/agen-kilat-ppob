import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../../services/authentication/authentication.service";
import {Router} from "@angular/router";
import {formatNumber} from "@angular/common";
import {ProductsModel} from "../../../models/products.model";
import {ProductService} from "../../../services/product/product.service";
import {ToastService} from "../../../services/toast/toast.service";
import {LoadingService} from "../../../services/loading/loading.service";
import {AlertService} from "../../../services/alert/alert.service";

@Component({
  selector: 'app-pln',
  templateUrl: './pln.page.html',
  styleUrls: ['./pln.page.scss'],
})
export class PlnPage implements OnInit {

  billID: string = '';
  category: string = '';
  products: ProductsModel[] = [];
  showData: boolean = false;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private productService: ProductService,
    public toastService: ToastService,
    private loadingService: LoadingService,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.billID = '';
    this.category = '';
    this.showData = false;
  }

  async getProductData() {
    this.products = [];
    await this.loadingService.present();
    try {
      const response = await this.productService.getProductData(this.category);
      response.data.forEach((val: any) => {
        val.priceNum = val.price;
        val.price = formatNumber(val.price, 'en-US', '1.0')
        this.products.push(val);
      });
      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e?.error?.message || 'Internal server error');
      // console.log('error is : ' + e.error.message);
      if (e?.error?.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async enterBillID() {
    if (!this.validateBillID()) {
      return;
    }
    const billIDSparate = this.billID.match(/.{1,4}/g);
    this.billID = billIDSparate ? billIDSparate.join('-') : '';
    if (this.category === 'PLNPRE001') {
      this.showData = true;
    } else {
      const params = JSON.stringify({bill_id: this.billID, product_code: this.products[0].code});
      await this.router.navigate(['/tabs/home/payment', {data: params}]);
    }
  }

  validateBillID() {
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    if (this.billID.length < 8) {
      this.alertService.presentAlert('Bad Request', 'id pelanggan harus lebih dari 8 angka')
        .then();
      return false;
    }
    return true;
  }

  async presentToastWithOptions(val: any) {
    if (!this.validateBillID()) {
      return;
    }
    const params = JSON.stringify({bill_id: this.billID, product_code: val.code, category: this.category});
    const billIDSparate = this.billID.match(/.{1,4}/g);
    this.billID = billIDSparate ? billIDSparate.join('-') : '';
    return await this.toastService.presentToastWithOptions(val.name, val.price, params);
    // console.log('toast', toast)
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
