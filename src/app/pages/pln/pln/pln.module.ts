import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PlnPageRoutingModule } from './pln-routing.module';

import { PlnPage } from './pln.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PlnPageRoutingModule
  ],
  declarations: [PlnPage]
})
export class PlnPageModule {}
