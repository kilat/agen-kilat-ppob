import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PlnPage } from './pln.page';

const routes: Routes = [
  {
    path: '',
    component: PlnPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PlnPageRoutingModule {}
