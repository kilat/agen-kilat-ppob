import { Component, OnInit } from '@angular/core';
import {format, parseISO} from 'date-fns';
import {id} from 'date-fns/locale';
import {LoadingService} from '../../services/loading/loading.service';
import {AlertService} from '../../services/alert/alert.service';
import {TrxService} from '../../services/trx/trx.service';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {TrxDataModel} from "../../models/trx.model";
import { PushNotifications } from '@capacitor/push-notifications';
import {Platform} from "@ionic/angular";

@Component({
  selector: 'app-trx',
  templateUrl: './trx.page.html',
  styleUrls: ['./trx.page.scss'],
})
export class TrxPage implements OnInit {

  modes = ['date', 'date-time', 'month', 'month-year', 'time', 'time-date', 'year'];
  // selectedMode = 'date-time';
  selectedMode = 'date';
  showDatePicker = false;
  showDatePicker2 = false;
  // dateValue = format(new Date(), 'yyyy-MM-dd') + 'T15:10:11.000Z';
  // this for date time
  // dateValue = format(new Date(), 'yyyy-MM-dd', { locale: id }) + 'T'+getHours(new Date())+':'+getMinutes(new Date())+':00.000Z';
  dateValue: string = '';
  dateValue2: string = '';
  formattedString = '';
  formattedString2 = '';
  hour = '00';
  minutes = '00';
  hour2 = '00';
  minutes2 = '00';
  params: {
    fromDate: string;
    toDate: string;
  } = {
    fromDate: '',
    toDate: '',
  };
  trx: TrxDataModel[] = [];

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private rrxService: TrxService,
    private plt: Platform,
  ) {
    // this.plt.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  async ionViewWillEnter() {
    if (this.plt.is('capacitor')) {
      await this.resetBadgeCount();
    }
    this.dateValue = format(new Date(), 'yyyy-MM-dd', { locale: id });
    this.dateValue2 = format(new Date(), 'yyyy-MM-dd', { locale: id });
    // alert(format(new Date(), 'yyyy-MM-dd') + 'T00:00:00.000Z');
    // const hour = getHours(new Date());
    // const minutes = getMinutes(new Date());
    // if (hour.toString().length === 1) {
    //   this.hour = '0' + hour.toString();
    // } else {
    //   this.hour = hour.toString();
    // }
    // if (minutes.toString().length === 1) {
    //   this.minutes = '0' + minutes.toString();
    // } else {
    //   this.minutes = minutes.toString();
    // }
    await this.setToday(this.hour, this.minutes);
    this.params.fromDate = this.dateValue;
    this.params.toDate = this.dateValue2;
    await this.getTrxData();
  }

  async getTrxData() {
    this.trx = [];
    await this.loadingService.present();
    try {
      const response = await this.rrxService.getTrxData(this.params);
      this.trx = response.data;
      for (const val of this.trx) {
        if(val.type === '00') {
          val.product_name = 'Manual Deposit'
          val.category_name = 'Deposit'
        }
        val.created_at = format(
          parseISO(format(new Date(val.created_at || '2000-14-11'), 'yyyy-MM-dd HH:mm')),
          'MMM d, yyyy HH:mm'
        );
      }
      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e.error.message);
      // console.log('error is : ' + e.error.message);
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async setToday(hour: any, m: any) {
    this.formattedString = format(
      // parseISO(format(new Date(), 'yyyy-MM-dd') + 'T'+hour+':'+m+':00+07:00'),
      parseISO(format(new Date(), 'yyyy-MM-dd')),
      // 'HH:mm, MMM d, yyyy',
      'MMM d, yyyy'
    );

    // always set same
    this.formattedString2 = this.formattedString;
  }

  async dateChanged(value: any) {
    // console.log(value);
    this.dateValue = value;
    // this.formattedString = format(parseISO(value), 'HH:mm, MMM d, yyyy');
    this.formattedString = format(parseISO(value), 'MMM d, yyyy');
    this.showDatePicker = false;
    this.params.fromDate = this.dateValue;
    await this.getTrxData();
  }

  async dateChanged2(value: any) {
    // console.log(value);
    this.dateValue2 = value;
    this.formattedString2 = format(parseISO(value), 'MMM d, yyyy');
    this.showDatePicker2 = false;
    this.params.toDate = this.dateValue2;
    await this.getTrxData();
  }

  async goToDet(data: any) {
    await this.router.navigate(['/tabs/trx/trx-detail', { dataDetail: JSON.stringify(data) }]);
  }

  ngOnInit(): void {
  }

  async doRefresh(event: any) {
    setTimeout(async () => {
      console.log('Async operation has ended');
      await this.ionViewWillEnter()
      event.target.complete();
    }, 1000);
  }

  async resetBadgeCount() {
    await PushNotifications.removeAllDeliveredNotifications();
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }
}
