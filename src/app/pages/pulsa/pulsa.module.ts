import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PulsaPageRoutingModule } from './pulsa-routing.module';

import { PulsaPage } from './pulsa.page';
import {ProdListPageModule} from "../modals/prodList/prod-list/prod-list.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PulsaPageRoutingModule,
    ProdListPageModule
  ],
  declarations: [PulsaPage]
})
export class PulsaPageModule {}
