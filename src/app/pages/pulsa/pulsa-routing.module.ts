import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PulsaPage } from './pulsa.page';

const routes: Routes = [
  {
    path: '',
    component: PulsaPage
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PulsaPageRoutingModule {}
