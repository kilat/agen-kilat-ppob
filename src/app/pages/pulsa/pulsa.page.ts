import { Component, OnInit } from '@angular/core';
import {ToastService} from "../../services/toast/toast.service";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../services/product/product.service";
import {LoadingService} from "../../services/loading/loading.service";
import {AlertService} from "../../services/alert/alert.service";
import { ProductsModel } from "../../models/products.model";
import {formatNumber} from "@angular/common";
import {ModalController, Platform} from "@ionic/angular";
import { ProdListPage } from "../modals/prodList/prod-list/prod-list.page";

@Component({
  selector: 'app-pulsa',
  templateUrl: './pulsa.page.html',
  styleUrls: ['./pulsa.page.scss'],
})
export class PulsaPage implements OnInit {

  products: ProductsModel[] = [];
  productsOrigin: ProductsModel[] = [];
  phoneNo: any = '';
  phoneNoVal: string = '';
  prefix: string = '';
  category: string = '';
  showDataAfterModal: boolean = false;
  catName: string = '';

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    public toastService: ToastService,
    private productService: ProductService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private plt: Platform,
    private modal: ModalController,
  ) {
    // this.plt.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.category = this.route.snapshot.paramMap.get('category') || '';
    if (this.category !== 'PULSE001' && this.category !== 'PAKETDATA001') {
      this.phoneNo = '';
      this.phoneNoVal = '';
      this.prefix = '';
      this.showDataAfterModal = false;
      this.catName = '';
      await this.getProductData(null);
      if (this.productsOrigin.length === 0) {
        this.productsOrigin = this.products;
      }
      return;
    } else {
      this.showDataAfterModal = true;
    }
  }

  async enterPhoneNo() {
    if (!this.validatePhoneNo()) {
      return;
    }
    const isNew = this.phoneNo.length > 12;
    const phoneNeSparate = this.phoneNo.match(/.{1,4}/g);
    this.phoneNo = phoneNeSparate.join('-');
    // console.log(this.phoneNo)


    // alert(isNew);
    const prefix = this.phoneNo.substring(0, 4);
    const prefixEnv = this.productService.providerList(prefix, isNew) || '';
    const prefixRes = prefixEnv.toUpperCase().replace('.', '') || '';
    if (this.category === 'EWALLET001' && this.showDataAfterModal) {
      // return await this.getProductData(null);
    } else {
      return await this.getProductData(prefixRes);
    }
  }

  async getProductData(prefixEnv: any) {
    this.products = [];
    await this.loadingService.present();
    try {
      const response = await this.productService.getProductData(this.category);
      // for async
      // for (const val of response.data) {
      //   if (prefixEnv.trim() === val.name.trim().split(' ')[0]) {
      //     val.price = formatNumber(val.price, 'id-ID', '1.0')
      //     this.products.push(val);
      //   }
      // }
      if (prefixEnv) {
        this.prefix = prefixEnv.trim();
      }

      response.data.forEach((val: any) => {
        // console.log(prefixEnv.trim().toUpperCase())
        // console.log(val.name.trim().split(' ')[0].toUpperCase())
        let brand = val?.brand?.toUpperCase() || null
        if (brand) {
          brand = brand.replace('THREE', 'TRI');
        } else {
          brand = '';
        }
        val.priceNum = val.price;
        val.price = formatNumber(val.price, 'en-US', '1.0')
        const name_cat = val.name.split(' ');
        val.name_cat = name_cat[0] + ' ' + name_cat[1] + ' ' + name_cat[2];
        if (!prefixEnv) {
          val.name_cat = name_cat[0] + ' ' + name_cat[1];
          if (val.name_cat === 'Saldo Gojek') {
            const name_cate_driver = name_cat[0] + ' ' + name_cat[1] + ' ' + name_cat[2];
            if (name_cate_driver.indexOf('Driver') > -1) {
              val.name_cat = name_cate_driver;
            }
          } else if (name_cat[1] + ' ' + name_cat[2] === 'DIAMOND FREE') {
            val.name_cat = name_cat[1] + ' ' + name_cat[2] + ' ' + name_cat[2];
          } else if (
            name_cat[0] === 'Ragnarok' || name_cat[0] === 'SAUSAGE' || name_cat[0] === 'Gamescool' ||
            name_cat[0] === 'Vidio.com'
          ) {
            val.name_cat = name_cat[0];
          } else if (val.name_cat === 'Arena of' || val.name_cat === 'Call of' || val.name_cat === 'Garena Free') {
            val.name_cat = name_cat[0] + ' ' + name_cat[1] + ' ' + name_cat[2];
          }
          this.products.push(val);
        } else if (prefixEnv.trim().toUpperCase() === val.name.trim().split(' ')[0].toUpperCase().replace('.', '')) {
          this.products.push(val);
        } else if (prefixEnv.trim().toUpperCase() === brand.toUpperCase() || null) {
          this.products.push(val);
        }
      });

      if (this.category === 'PAKETDATA001') {
        // @ts-ignore
        this.products.sort((b, a) => a.name_cat.localeCompare(b.name_cat) || b.priceNum - a.priceNum);
      }

      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e?.error?.message || 'Internal server error');
      // console.log('error is : ' + e.error.message);
      if (e?.error?.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async presentToastWithOptions(val: any) {
    if (!this.validatePhoneNo()) {
      return;
    }
    const params = JSON.stringify({bill_id: this.phoneNo, product_code: val.code});
    const phoneNeSparate = this.phoneNo.match(/.{1,4}/g);
    this.phoneNo = phoneNeSparate.join('-');
    return await this.toastService.presentToastWithOptions(val.name, val.price, params);
    // console.log('toast', toast)
  }

  validatePhoneNo() {
    this.phoneNo = this.phoneNo.replace('-', '');
    this.phoneNo = this.phoneNo.replace('-', '');
    this.phoneNo = this.phoneNo.replace('-', '');
    this.phoneNo = this.phoneNo.replace('-', '');
    if (this.phoneNo.length < 8) {
      this.alertService.presentAlert('Bad Request', 'nomor telepon terlalu sedikit, minimal 8 angka')
        .then();
      return false;
    }
    return true;
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

  async dummyTest() {
    if (this.productsOrigin.length !== this.products.length) {
      this.showDataAfterModal = false;
      this.products = this.productsOrigin;
    }

    const modal = await this.modal.create({
      component: ProdListPage,
      componentProps: { isModal: true, products: this.productsOrigin },
    });

    await modal.present();

    const { data } = await modal.onWillDismiss();
    // this.custData = await data.data;
    this.catName = data?.data?.brand;
    // console.log(this.products)
    if (this.catName) {
      this.products = this.products.filter(value => {
        // console.log('name_cat', name_cat)
        // console.log('value.name_cat', value.name_cat)
        return value.brand === this.catName;
      });
      // @ts-ignore
      this.products.sort((b, a) => a.brand.localeCompare(b.brand) || b.priceNum - a.priceNum);
      // console.log(this.products);
      this.showDataAfterModal = true;
    } else {
      this.catName = '';
      this.showDataAfterModal = false;
    }

    if (window.history.state.modal) {
      const modalState = { modal: true };
      // @ts-ignore
      history.pushState(modalState, null);
    }
  }
}
