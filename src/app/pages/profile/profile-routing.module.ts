import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfilePage } from './profile.page';
import {PrintSettingPage} from "./print-setting/print-setting.page";

const routes: Routes = [
  {
    path: '',
    component: ProfilePage
  },
  {
    path: 'printer-settings',
    component: PrintSettingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfilePageRoutingModule {}
