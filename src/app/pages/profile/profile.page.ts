import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {Router} from "@angular/router";
import {UserService} from "../../services/user/user.service";
import {LoadingService} from "../../services/loading/loading.service";
import {AlertService} from "../../services/alert/alert.service";
import {UserDataModel} from "../../models/users.model";
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  user: UserDataModel = new UserDataModel();

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private userService: UserService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private androidPermissions: AndroidPermissions,
  ) { }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.user = new UserDataModel();
    await this.loadingService.present();
    try {
      const response = await this.userService.getUserData();
      this.user = response.data;
      await this.loadingService.dismiss();

      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH_SCAN).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH_SCAN)
      );

      this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.BLUETOOTH_CONNECT).then(
        result => console.log('Has permission?',result.hasPermission),
        err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.BLUETOOTH_CONNECT)
      );

      this.androidPermissions.requestPermissions([
        this.androidPermissions.PERMISSION.BLUETOOTH_CONNECT,
        this.androidPermissions.PERMISSION.BLUETOOTH_SCAN,
        this.androidPermissions.PERMISSION.BLUETOOTH_ADMIN,
        this.androidPermissions.PERMISSION.BLUETOOTH,
        this.androidPermissions.PERMISSION.BLUETOOTH_ADVERTISE,
        this.androidPermissions.PERMISSION.BLUETOOTH_PRIVILEGED,
      ]).then();

    } catch (e: any) {
      await this.loadingService.dismiss();
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
      await this.alertService.presentAlert('Error', e.error.message);
    }
  }

  async saveProfile() {
    if (this.user.password !== this.user.password_con) {
      await this.alertService.presentAlert('Error', 'password baru dan konfirmasi password tidak sama');
      return
    }

    await this.loadingService.present();
    try {
      const id = this.user._id;
      const response = await this.userService.updateUser(this.user, id);
      if (response.message === 'updated') {
        await this.loadingService.dismiss();
        await this.alertService.presentAlert('Success', 'Update data berhasil');
        await this.ionViewWillEnter();
        return;
      }
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', 'Update data error');
    } catch (e: any) {
      await this.loadingService.dismiss();
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
      await this.alertService.presentAlert('Error', e.error.message);
      await this.ionViewWillEnter();
    }
  }

  async goToPrinterSettings() {
    await this.router.navigate(['/tabs/profile/printer-settings', {}]);
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
