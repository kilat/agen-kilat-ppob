import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PrintSettingPageRoutingModule } from './print-setting-routing.module';

import { PrintSettingPage } from './print-setting.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrintSettingPageRoutingModule
  ],
  declarations: [PrintSettingPage]
})
export class PrintSettingPageModule {}
