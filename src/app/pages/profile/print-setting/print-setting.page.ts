import { Component, OnInit } from '@angular/core';
import { Storage } from '@capacitor/storage';
import {PrintSettingService} from "../../../services/print-setting/print-setting.service";
import {AuthenticationService} from "../../../services/authentication/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-print-setting',
  templateUrl: './print-setting.page.html',
  styleUrls: ['./print-setting.page.scss'],
})
export class PrintSettingPage implements OnInit {

  bluetoothList: any = [];
  selectedPrinter: any;
  printerMacAddress: string = 'printerMcAddress';

  constructor(
    private print:PrintSettingService,
    private authService: AuthenticationService,
    private router: Router,
  ) { }

  async ngOnInit() {
    this.listPrinter();
    if (!this.selectedPrinter) {
      const printerMcAddress = await Storage.get({key: this.printerMacAddress});
      if (printerMcAddress && printerMcAddress.value) {
        this.selectedPrinter = printerMcAddress.value;
      }
    }
  }

  listPrinter() {
    this.print.searchBluetoothPrinter()
    .then(resp=>{
      //List of bluetooth device list
      this.bluetoothList=resp;
    });
  }

  //This will store selected bluetooth device mac address
  async selectPrinter(macAddress: string)
  {
    try {
      await Storage.set({key: this.printerMacAddress, value: macAddress});
      alert('Printer Setting Success');
      this.selectedPrinter = macAddress;
    }catch (e) {
      alert('Printer Setting Failed');
    }
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
