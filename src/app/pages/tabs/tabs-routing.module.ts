import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';
import {AuthGuard} from '../../guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'home',
        loadChildren: () => import('../home/home.module').then( m => m.HomePageModule),
        canLoad: [AuthGuard] // Secure all child pages
      },
      {
        path: 'home/pulsa',
        loadChildren: () => import('../pulsa/pulsa.module').then( m => m.PulsaPageModule),
        canLoad: [AuthGuard] // Secure all child pages
      },
      {
        path: 'home/pln',
        loadChildren: () => import('../pln/pln/pln.module').then( m => m.PlnPageModule)
      },
      {
        path: 'home/postpaid',
        loadChildren: () => import('../postpaid/postpaid.module').then( m => m.PostpaidPageModule)
      },
      {
        path: 'deposit',
        loadChildren: () => import('../deposit/deposit.module').then( m => m.DepositPageModule),
        canLoad: [AuthGuard] // Secure all child pages
      },
      {
        path: 'trx',
        loadChildren: () => import('../trx/trx.module').then( m => m.TrxPageModule),
        canLoad: [AuthGuard] // Secure all child pages
      },
      {
        path: 'trx/trx-detail',
        loadChildren: () => import('../trx-detail/trx-detail.module').then( m => m.TrxDetailPageModule),
        canLoad: [AuthGuard] // Secure all child pages
      },
      {
        path: 'home/payment',
        loadChildren: () => import('../payment/payment.module').then(m => m.PaymentPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('../profile/profile.module').then( m => m.ProfilePageModule)
      },
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/home',
    pathMatch: 'full',
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TabsPageRoutingModule {}
