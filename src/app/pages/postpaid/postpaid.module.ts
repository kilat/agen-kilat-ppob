import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostpaidPageRoutingModule } from './postpaid-routing.module';

import { PostpaidPage } from './postpaid.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostpaidPageRoutingModule
  ],
  declarations: [PostpaidPage]
})
export class PostpaidPageModule {}
