import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostpaidPage } from './postpaid.page';

const routes: Routes = [
  {
    path: '',
    component: PostpaidPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostpaidPageRoutingModule {}
