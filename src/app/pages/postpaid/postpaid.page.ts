import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ProductService} from "../../services/product/product.service";
import {ToastService} from "../../services/toast/toast.service";
import {LoadingService} from "../../services/loading/loading.service";
import {AlertService} from "../../services/alert/alert.service";
import {ProductsModel} from "../../models/products.model";
import {formatNumber} from "@angular/common";

@Component({
  selector: 'app-postpaid',
  templateUrl: './postpaid.page.html',
  styleUrls: ['./postpaid.page.scss'],
})
export class PostpaidPage implements OnInit {

  billID: string = '';
  category: string = '';
  products: ProductsModel[] = [];
  showData: boolean = false;
  prodCode: string = '';
  // prodName: string;

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private route: ActivatedRoute,
    private productService: ProductService,
    public toastService: ToastService,
    private loadingService: LoadingService,
    private alertService: AlertService,
  ) { }

  ngOnInit() {
  }

  async ionViewDidEnter() {
    // this.prodName = '';
    this.billID = '';
    this.prodCode = '';
    this.category = this.route.snapshot.paramMap.get('category') || '';
    this.products = [];
    this.showData = false;
    await this.getProductData();
    if (this.products.length === 1) {
      this.prodCode = this.products[0].code || '';
    }
  }

  async getProductData() {
    this.products = [];
    await this.loadingService.present();
    try {
      const response = await this.productService.getProductData(this.category);
      response.data.forEach((val: any) => {
        val.priceNum = val.price;
        val.price = formatNumber(val.price, 'en-US', '1.0')
        this.products.push(val);
      });
      await this.loadingService.dismiss();
    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e?.error?.message || 'Internal server error');
      // console.log('error is : ' + e.error.message);
      if (e?.error?.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async enterBillID() {
    if (!this.validateBillID()) {
      return;
    }
    const billIDSparate = this.billID.match(/.{1,4}/g);
    this.billID = billIDSparate ? billIDSparate.join('-') : '';

    const params = JSON.stringify({bill_id: this.billID, product_code: this.prodCode, category: this.category});
    await this.router.navigate(['/tabs/home/payment', {data: params}]);

  }

  validateBillID() {
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    this.billID = this.billID.replace('-', '');
    if (this.billID.length < 8) {
      this.alertService.presentAlert('Bad Request', 'id pelanggan harus lebih dari 8 angka')
        .then();
      return false;
    }
    return true;
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
