import { Component, OnInit } from '@angular/core';
import {TrxDataModel} from "../../models/trx.model";
import {ActivatedRoute, Router} from "@angular/router";
import {formatNumber} from "@angular/common";
import {AuthenticationService} from "../../services/authentication/authentication.service";
import {Storage} from "@capacitor/storage";
import {UserDataModel} from "../../models/users.model";
import {UserService} from "../../services/user/user.service";
import {PrintSettingService} from "../../services/print-setting/print-setting.service";
import {AlertController} from "@ionic/angular";
// import {Platform} from "@ionic/angular";

@Component({
  selector: 'app-trx-detail',
  templateUrl: './trx-detail.page.html',
  styleUrls: ['./trx-detail.page.scss'],
})
export class TrxDetailPage implements OnInit {

  setUndefined: string = '';
  trxData: TrxDataModel = new TrxDataModel();
  user: UserDataModel = new UserDataModel();
  amount: string = '';
  admin: string = '';
  total_amount: string = '';
  first_balance: string = '';
  last_balance: string = '';
  printerMacAddress: string = 'printerMcAddress';
  pulsaCat: string[] = [
    'PULSE001',
    'PAKETDATA001',
    'GAME001',
    'PLNPRE001',
    'PLNPOS001',
    'EWALLET001'
  ];
  inputAmountTotal: string = '';

  constructor(
    private authService: AuthenticationService,
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserService,
    private print: PrintSettingService,
    public alertController: AlertController,
    // private plt: Platform,
  ) {
    // this.plt.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  ngOnInit() {
  }

  async ionViewWillEnter() {
    this.trxData = JSON.parse(<string>this.route.snapshot.paramMap.get('dataDetail')) || '';
    this.amount = formatNumber(this.trxData.amount || 0, 'en-US', '1.0');
    this.admin = formatNumber(this.trxData.admin || 0, 'en-US', '1.0');
    this.total_amount = formatNumber(this.trxData.total_amount || 0, 'en-US', '1.0');
    this.first_balance = formatNumber(this.trxData.first_balance || 0, 'en-US', '1.0');
    this.last_balance = formatNumber(this.trxData.last_balance || 0, 'en-US', '1.0');
    this.user = await this.getUser();
  }

  async printRequest() {
    if (this.trxData?.category_code === 'PDAM001') {
      await this.printSuccessPDAM();
    } else if (this.pulsaCat.includes(<string>this.trxData?.category_code)) {
      await this.presentAlertConfirm();
    }
  }

  async presentAlertConfirm() {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirmation',
      // message: 'Are you sure to pay this transaction ? <br /> phone no : ' + this.billIdFormat(this.billingId),
      inputs: [
        {
          placeholder: 'Input harga jual',
          name: 'inputAmountTotal'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (alertData) => {
            console.log('Confirm Cancel: blah', alertData);
          }
        }, {
          text: 'Okay',
          handler: async (alertData) => {
            console.log('Confirm Okay', alertData);
            this.inputAmountTotal = alertData.inputAmountTotal;
            await this.printSuccessPulsa();
          }
        }
      ]
    });

    await alert.present();
  }

  async getUser() {
    const response = await this.userService.getUserData();
    return await response.data;
  }

  async printSuccessPDAM() {
    let myText = await this.headerPrint();

    myText += 'TRX Id : ' + this.trxData.tx_id + '\n';
    myText += 'PDAM : ' + this.trxData.product_name + '\n';
    myText += 'Id Pelanggan : ' + this.trxData.bill_id + ' \n';
    // @ts-ignore
    myText += 'Nama : ' + this.trxData?.res_param.detail?.cust_name || '' + '\n';
    myText += 'Tanggal : ' + this.trxData.created_at + ' \n';
    // @ts-ignore
    myText += 'Jml Bln : ' + this.trxData.res_param.detail.bill_id || '-' + ' \n';
    myText += 'Tot Tagihan : ' + formatNumber(Number(this.trxData.amount), 'en-US', '1.0-0') + ' \n';
    myText += 'Tot Admin : ' + formatNumber(Number(this.trxData.admin), 'en-US', '1.0-0') + ' \n';
    myText += 'Total : ' + formatNumber(Number(this.trxData.total_amount), 'en-US', '1.0-0') + ' \n\n';

    myText += await this.footerPrint();

    console.log(myText);

    const printer = await Storage.get({ key: this.printerMacAddress });
    if (printer && printer.value) {
      this.print.sendToBluetoothPrinter(printer.value, myText);
    }
  }

  async printSuccessPulsa() {
    let myText = await this.headerPrint();

    myText += 'TRX Id : ' + this.trxData.tx_id + '\n';
    myText += 'Id Pelanggan : ' + this.trxData.bill_id + ' \n';
    myText += 'SN : ' + this.trxData.sn + '\n';
    myText += 'Tanggal : ' + this.trxData.created_at + ' \n';
    myText += 'Tot Tagihan : ' + formatNumber(Number(this.inputAmountTotal || '0'), 'en-US', '1.0-0') + ' \n';
    myText += 'Tot Admin : ' + formatNumber(Number('0'), 'en-US', '1.0-0') + ' \n';
    myText += 'Total : ' + formatNumber(Number(this.inputAmountTotal || '0'), 'en-US', '1.0-0') + ' \n\n';

    myText += await this.footerPrint();

    console.log(myText);

    const printer = await Storage.get({ key: this.printerMacAddress });
    if (printer && printer.value) {
      // console.log('set token: ', token.value);
      this.print.sendToBluetoothPrinter(printer.value, myText);
    }
  }

  async headerPrint() {
    let myText = 'Kilat Telknologi Mandiri \n';
    myText += this.trxData.created_at + ' \n';
    myText += 'Loket : ' + this.user.full_name + ' \n\n\n';

    return myText;
  }

  async footerPrint() {
    let myText = 'Menyatakan Struk Ini Adalah Bukti Pembayaran Yang Sah \n\n';

    myText += 'Terimakasih \n';
    myText += 'Kilat Teknologi Mandiri \n\n\n\n\n';

    return myText;
  }

  async backToList() {
    await this.router.navigate(['/tabs/trx']);
  }

  async doRefresh(event: any) {
    setTimeout(async () => {
      console.log('Async operation has ended');
      await this.ionViewWillEnter()
      event.target.complete();
    }, 1000);
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
