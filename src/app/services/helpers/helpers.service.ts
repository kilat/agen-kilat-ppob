import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HelpersService {

  constructor() { }

  billIdFormat(val: any){
    let newValue: any = val;
    if (val.indexOf(',') > -1) {
      newValue = val.replaceAll(',', '');
    }
    if (newValue.length > 9) {
      newValue = newValue.replace(',', '');
      val = newValue.substr(0, newValue.length - 9) +
        ',' + newValue.substr(newValue.length - 9, 3) +
        ',' + newValue.substr(newValue.length - 6, 3) +
        ',' + newValue.substr(newValue.length - 3);
    }else if (newValue.length > 6) {
      newValue = newValue.replace(',', '');
      val = newValue.substr(0, newValue.length - 6) +
        ',' + newValue.substr(newValue.length - 6, 3) +
        ',' + newValue.substr(newValue.length - 3);
    } else if (newValue.length > 3) {
      newValue = newValue.replace(',', '');
      val = newValue.substr(0, newValue.length - 3) +
        ',' + newValue.substr(newValue.length - 3);
    } else {
      val = newValue;
    }

    return val;
  }
}
