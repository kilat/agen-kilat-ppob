import { Injectable } from '@angular/core';
import { PushNotifications } from '@capacitor/push-notifications';
// import {Platform} from "@ionic/angular";
import {Storage} from "@capacitor/storage";
// import {environment} from "../../../environments/environment";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(
    // private platform: Platform,
    private router: Router
  ) { }

  addListeners = async () => {
    await PushNotifications.addListener('registration', token => {
      console.info('Registration token: ', token.value);
      Storage.set({key: 'notifyToken', value: token.value});
      // alert('Registration token: ' + token.value)
    });

    await PushNotifications.addListener('registrationError', err => {
      console.error('Registration error: ', err.error);
    });

    await PushNotifications.addListener('pushNotificationReceived', notification => {
      console.log('Push notification received: ', JSON.stringify(notification));
    });

    await PushNotifications.addListener('pushNotificationActionPerformed', notification => {
      console.log('Push notification action performed', JSON.stringify(notification));
      this.router.navigateByUrl(`/tabs/trx`);
      // this.router.navigateByUrl(`/home`);
    });
  }

  registerNotifications = async () => {
    let permStatus = await PushNotifications.checkPermissions();

    if (permStatus.receive === 'prompt') {
      permStatus = await PushNotifications.requestPermissions();
    }

    if (permStatus.receive !== 'granted') {
      alert('User denied permissions!')
      throw new Error('User denied permissions!');
    }

    await PushNotifications.register();
  }

  getDeliveredNotifications = async () => {
    const notificationList = await PushNotifications.getDeliveredNotifications();
    console.log('delivered notifications', notificationList);
    // alert('delivered notifications ' + notificationList)
  }
}
