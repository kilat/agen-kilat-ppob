import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import {from, throwError} from 'rxjs';
import {Storage} from '@capacitor/storage';
import {RequestDepositModel, UserDataModel} from '../../models/users.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  token: string = '';

  constructor(private http: HttpClient) {}

  async getUserData() {
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    return this.http.get(environment.apiUrl + 'users/who-am-i', header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        tap(async (result) => {
          // console.log(result);
          await Storage.remove({key: 'user'});
          await this.setUser(result.data);
        }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async setUserToken(id: string) {
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    const tokenPush = await Storage.get({ key: 'notifyToken' });

    const params = {
      token: tokenPush.value
    };

    return this.http.put(environment.apiUrl + 'users/client', params, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async updateUser(params: UserDataModel, id: string) {
    // const token = await Storage.get({ key: environment.tokenKey });
    // this.token = token.value;
    const header = await this.getToken();
    // @ts-ignore
    delete params._id;
    delete params.created_at;
    delete params.created_by;
    delete params.password_con;

    return this.http.put(environment.apiUrl + 'users/' + id, params, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async saveDepositUser(params: RequestDepositModel) {
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    return this.http.post(environment.apiUrl + 'transactions', params, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        catchError(this.errorHandler)
      ).toPromise();
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError( error || 'server error.' );
  }

  async getToken(){
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token?.value || '';
    return {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };
  }

  // JSON "set" example
  async setUser(user: UserDataModel) {
    await Storage.set({
      key: 'user',
      value: JSON.stringify(user)
    });
  }


}
