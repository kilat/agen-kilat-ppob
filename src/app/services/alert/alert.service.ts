import { Injectable } from '@angular/core';
import {AlertController} from '@ionic/angular';
import { App as CapacitorApp } from '@capacitor/app';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(public alertController: AlertController) { }

  async presentAlert(header: any, message: any) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header,
      // subHeader: 'Subtitle',
      message,
      buttons: ['OK']
    });

    await alert.present();

    // const { role } = await alert.onDidDismiss();
    // console.log('onDidDismiss resolved with role', role);
  }

  async presentAlertConfirmation(header: string, message: string, exit: boolean = false) {
    const alert = await this.alertController.create({
      // cssClass: 'my-custom-class',
      header,
      // subHeader: 'Subtitle',
      message,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
        },
        {
          text: 'Tutup Aplikasi',
          handler: () => {
            if (exit) {
              // @ts-ignore
              navigator["app"].exitApp();
              // CapacitorApp.addListener('backButton', ({canGoBack}) => {
              //     CapacitorApp.exitApp();
              // });
            }
          }
        }
      ]
    });

    await alert.present();

    // const { role } = await alert.onDidDismiss();
    // console.log('onDidDismiss resolved with role', role);
  }
}
