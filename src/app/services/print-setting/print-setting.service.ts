import { Injectable } from '@angular/core';
import { BluetoothSerial } from '@awesome-cordova-plugins/bluetooth-serial/ngx';
import { AndroidPermissions } from '@awesome-cordova-plugins/android-permissions/ngx';

@Injectable({
  providedIn: 'root'
})
export class PrintSettingService {

  constructor(
      public btSerial: BluetoothSerial,
      private androidPermissions: AndroidPermissions,
  ) { }

  searchBluetoothPrinter()
  {
    this.androidPermissions.requestPermissions([
      this.androidPermissions.PERMISSION.BLUETOOTH_CONNECT,
      this.androidPermissions.PERMISSION.BLUETOOTH_SCAN,
    ]).then();
    // This will return a list of bluetooth devices
    return this.btSerial.list();
  }
  connectToBluetoothPrinter(macAddress: string)
  {
    // This will connect to bluetooth printer via the mac address provided
    return this.btSerial.connect(macAddress);
  }
  disconnectBluetoothPrinter()
  {
    // This will disconnect the current bluetooth connection
    return this.btSerial.disconnect();
  }

  // macAddress->the device's mac address
  // data_string-> string to be printer
  sendToBluetoothPrinter(macAddress: string, data_string: string)
  {
    // 1. Try connecting to bluetooth printer
    this.connectToBluetoothPrinter(macAddress)
    .subscribe((_: any) => {
        // 2. Connected successfully
        this.btSerial.write(data_string)
        .then((_: any) => {
          console.log(_)
        // 3. Print successful
        // If you want to tell user print is successful,
        // handle it here
        // 4. IMPORTANT! Disconnect bluetooth after printing
        this.disconnectBluetoothPrinter().then();
        }, (err: any) => {
          console.log(err);
          // If there is an error printing to bluetooth printer
          alert('If there is an error printing to bluetooth printer');
          // handle it here
        });
    }, (err: any) => {
      console.log(err);
      // If there is an error connecting to bluetooth printer
      alert('If there is an error connecting to bluetooth printer');
      // handle it here
    });
  }
}
