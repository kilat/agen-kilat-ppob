import { TestBed } from '@angular/core/testing';

import { PrintSettingService } from './print-setting.service';

describe('PrintService', () => {
  let service: PrintSettingService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PrintSettingService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
