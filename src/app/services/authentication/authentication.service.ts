import { Injectable } from '@angular/core';
import {BehaviorSubject, from, Observable, throwError} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import { environment } from '../../../environments/environment';
import {catchError, map, switchMap, tap, timeout} from 'rxjs/operators';
import { Storage } from '@capacitor/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  // isAuthenticated: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isAuthenticated = new BehaviorSubject<boolean | null>(null);
  token = '';

  constructor(private http: HttpClient) {
    this.loadToken().then();
  }

  async loadToken() {
    const token = await Storage.get({ key: environment.tokenKey });
    if (token && token.value) {
      console.log('set token: ', token.value);
      this.token = token.value;
      this.isAuthenticated.next(true);
    } else {
      this.isAuthenticated.next(false);
    }
  }

  login(credentials: {email: string; password: string}): Observable<any> {
    return this.http.post(environment.apiUrl + 'login', credentials).pipe(
      timeout(30000),
      map((data: any) => data),
      switchMap(data =>
        // console.log(data);
         from([data])
      ),
      tap(async _ => {
        if (_.tokenData && _.data.role.level === 3 && _.data.bank_code === environment.appName) {
          await this.setUser(_.data);
          await Storage.set({key: environment.tokenKey, value: _.tokenData.token});
          await Storage.set({key: environment.tokenKeyRefresh, value: _.refToken.token});
          this.isAuthenticated.next(true);
        } else {
          // console.log(_.tokenData.token);
          this.isAuthenticated.next(false);
        }
      }),
      catchError(this.errorHandler)
    );
  }

  loginByGoogle(googleAuth: any): Observable<any> {
    if (googleAuth.serverAuthCode) {
      delete googleAuth.serverAuthCode;
    }
    googleAuth.bank_code = environment.appName
    // console.log(googleAuth)
    return this.http.post(environment.apiUrl + 'users/create-by-google', googleAuth).pipe(
      timeout(30000),
      map((data: any) => data),
      switchMap(data => {
        // console.log("masuk");
        return from([data]);
      }),
      tap(async _ => {
        // console.log(_.data)
        if (_.data.result_cd === '00') {
          // await this.setUser(_.data);
          await Storage.set({key: environment.tokenKey, value: _.data.token.token});
          await Storage.set({key: environment.tokenKeyRefresh, value: _.data.ref_token.token});
          this.isAuthenticated.next(true);
        } else {
          // console.log(_.tokenData.token);
          this.isAuthenticated.next(false);
        }
      }),
      catchError(this.errorHandler)
    );
  }

  // JSON "set" example
  async setUser(user: any) {
    await Storage.set({
      key: 'user',
      value: JSON.stringify(user)
    });
  }

  async logout(): Promise<void> {
    this.isAuthenticated.next(false);
    await Storage.remove({key: environment.tokenKeyRefresh});
    await Storage.remove({key: 'user'});
    return Storage.remove({key: environment.tokenKey});
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError( error || 'server error.' );
  }

}
