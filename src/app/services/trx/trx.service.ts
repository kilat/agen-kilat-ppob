import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Storage} from '@capacitor/storage';
import {environment} from '../../../environments/environment';
import {catchError, map, switchMap, timeout} from 'rxjs/operators';
import {from, throwError} from 'rxjs';
import {UserDataModel} from '../../models/users.model';
import {InqReqModel, PayReqModel} from "../../models/trx.model";
import * as CryptoJS from 'crypto-js';

@Injectable({
  providedIn: 'root'
})
export class TrxService {

  token: string = '';

  constructor(private http: HttpClient) { }

  async getTrxData(reqParams: { fromDate: string; toDate: string }, userParams: any = null) {
    const token = await Storage.get({ key: environment.tokenKey });
    let user: UserDataModel;
    if (!userParams) {
      const userStr = await Storage.get({ key: 'user' });
      user = JSON.parse(userStr?.value || '');
    } else {
      user = userParams;
    }

    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    let queryParams = 'from_date=' + reqParams.fromDate + '&to_date=' + reqParams.toDate;
    // eslint-disable-next-line no-underscore-dangle
    queryParams += '&user_id=' + user._id;
    queryParams += '&type=00,01';
    return this.http.get(environment.apiUrl + 'transactions?' + queryParams, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        // tap(result => {
        //   console.log(result);
        // }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async reqInq(reqParams: { bill_id: string; product_code: string; amount: number }) {
    // console.log(reqParams)
    const { token, user } = await this.setTokenAndUser();
    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    const timeStamp: string = await this.setTimeStamp();
    const ref_id: string = await this.setRefID();
    const bodyParams: InqReqModel = {
      timeStamp: timeStamp,
      appName: environment.appName,
      bill_id: reqParams.bill_id,
      merchantToken: '',
      product_code: reqParams.product_code,
      ref_id: ref_id,
      user_id: user._id,
    }

    if (reqParams?.amount) {
      if (reqParams.amount > 0) {
        bodyParams.amount = reqParams.amount;
      }
    }

    bodyParams.merchantToken = await this.setMerchantCode(bodyParams);

    return this.http.post(environment.apiUrl + 'inq', bodyParams, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        // tap(result => {
        //   console.log(result);
        // }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async reqPay(reqParams: PayReqModel) {
    // console.log(reqParams)
    const { token, user } = await this.setTokenAndUser();
    this.token = token?.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    // const timeStamp: string = await this.setTimeStamp();
    // const ref_id: string = await this.setRefID();
    // const bodyParams: PayReqModel = {
    //   timeStamp: timeStamp,
    //   appName: environment.appName,
    //   bill_id: reqParams.bill_id,
    //   merchantToken: '',
    //   product_code: reqParams.product_code,
    //   ref_id: ref_id,
    //   user_id: user._id,
    //   amount: 0,
    //   admin: 0,
    //   total_amount: 0
    // }
    // bodyParams.merchantToken = await this.setMerchantCode(bodyParams);

    return this.http.post(environment.apiUrl + 'pay', reqParams, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        // tap(result => {
        //   console.log(result);
        // }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async setTimeStamp(): Promise<string> {
    const getDate = await this.getDate();
    return getDate.year.toString() + this.pad2( getDate.month + 1 ) + this.pad2( getDate.day ) +
      this.pad2( getDate.hours ) + this.pad2( getDate.minutes ) + this.pad2( getDate.seconds );
  }

  async setRefID(): Promise<string> {
    const getDate = await this.getDate();
    return "ref_" + getDate.year.toString() + this.pad2( getDate.month + 1 ) +
      this.pad2( getDate.day ) + this.pad2( getDate.hours ) + this.pad2( getDate.minutes ) +
      this.pad2( getDate.seconds );
  }

  async setMerchantCode(bodyParams: InqReqModel): Promise<string> {
    // console.log(bodyParams.timeStamp)
    // console.log(bodyParams.appName)
    // console.log(bodyParams.ref_id)
    // console.log(bodyParams.bill_id)
    // console.log(bodyParams.product_code)
    // console.log(environment.secretCode)
    // console.log(bodyParams.user_id)
    const concat: string = bodyParams.timeStamp.concat(
      bodyParams.appName,
      bodyParams.ref_id,
      bodyParams.bill_id,
      bodyParams.product_code,
      environment.secretCode,
      bodyParams.user_id
    );

    return CryptoJS.SHA256(concat).toString(CryptoJS.enc.Hex)
    // return sha256(concat).toString;
  }

  pad2(n: number): string {
    return n < 10 ? '0' + n.toString() : n.toString();
  }

  async getDate(): Promise<{ year: number; month: number; day: number; hours: number; minutes: number; seconds: number }> {
    const date = new Date();
    const year = date.getFullYear();
    const month = date.getMonth();
    const day = date.getDate();
    const hours = date.getHours();
    const minutes = date.getMinutes();
    const seconds = date.getSeconds();

    return {year, month, day, hours, minutes, seconds};
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError( error || 'server error.' );
  }

  async setTokenAndUser () {
    const token = await Storage.get({ key: environment.tokenKey });
    const userStr = await Storage.get({ key: 'user' });
    const user: UserDataModel = JSON.parse(userStr?.value || '');

    return { token, user };
  }
}
