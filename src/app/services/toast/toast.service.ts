import { Injectable } from '@angular/core';
import {ToastController} from "@ionic/angular";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  isPresent = false;

  constructor(
    public toastController: ToastController,
    private router: Router,
  ) { }

  async presentToastWithOptions(header: any, message: any, val: any) {
    if (this.isPresent) {
      await this.dismiss();
    }
    this.isPresent = true;
    return await this.toastController.create({
      header,
      message,
      icon: 'information-circle',
      position: 'bottom',
      buttons: [
        // {
        //   side: 'start',
        //   icon: 'star',
        //   text: 'Favorite',
        //   handler: () => {
        //     console.log('Favorite clicked');
        //   }
        // },
        {
          text: 'Cancel',
          cssClass: 'cancelButton',
          handler: async () => {
            console.log('Cancel clicked');
            // await this.dismiss()
            this.isPresent = false;
          }
        },
        {
          text: 'Done',
          role: val,
          handler: async () => {
            console.log('Done clicked');
            // await this.dismiss()
            this.isPresent = false;

          }
        },
      ]
    }).then(a => {
      a.present().then(async () => {
        if (!this.isPresent) {
          a.dismiss().then(() => console.log('abort presenting'));
        }

        // console.log('onDIdiDissmiss')

        const { role } = await a.onDidDismiss();
        if (role) {
          // console.log('onDidDismiss resolved with role', role);
          // const roleParse = JSON.parse(role)
          await this.router.navigate(['/tabs/home/payment', {data: role}]);
        }
      })
    });
    //
    // const { role } = await toast.onDidDismiss();
    // console.log('onDidDismiss resolved with role', role);
  }

  async dismiss() {
    this.isPresent = false;
    return await this.toastController.dismiss().then(() => console.log('dismissed'));
  }
}
