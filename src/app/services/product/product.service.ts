import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Storage} from "@capacitor/storage";
import {environment} from "../../../environments/environment";
import {UserDataModel} from "../../models/users.model";
import {catchError, map, switchMap, timeout} from "rxjs/operators";
import {from, throwError} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  token: string = '';

  constructor(private http: HttpClient) { }

  async getProductData(category: any) {
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    let queryParams = 'category=' + category;
    return this.http.get(environment.apiUrl + 'products?' + queryParams, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        // tap(result => {
        //   console.log(result);
        // }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  async getProductByID(tx_id: string) {
    const token = await Storage.get({ key: environment.tokenKey });
    this.token = token.value || '';
    const header = {
      headers: {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        Authorization: 'Bearer ' + this.token,
      }
    };

    return this.http.get(environment.apiUrl + 'products/' + tx_id, header)
      .pipe(
        timeout(30000),
        map((data: any) => data),
        switchMap(data =>
          from([data])
        ),
        // tap(result => {
        //   console.log(result);
        // }),
        catchError(this.errorHandler)
      ).toPromise();
  }

  // @ts-ignore
  providerList(prefix, isNewPrf = false) {
    const TELKOMSEL = [
      '0811',
      '0812',
      '0813',
      '0821',
      '0822',
      '0823',
      '0852',
      '0853',
    ];

    const BYU = [
      '0851',
    ];

    const INDOSAT = [
      '0814',
      '0815',
      '0816',
      '0855',
      '0856',
      '0857',
      '0858'
    ];

    const XL = [
      '0817',
      '0818',
      '0819',
      '0859',
      '0877',
      '0878'
    ];

    const AXIS = [
      '0831',
      '0832',
      '0833',
      '0838'
    ];

    const THREE = [
      '0895',
      '0896',
      '0897',
      '0898',
      '0899'
    ];

    const SMART = [
      '0881',
      '0882',
      '0883',
      '0884',
      '0885',
      '0886',
      '0887',
      '0888',
      '0889'
    ];

    if (isNewPrf) {
      const AXISNEW = [
        '0831',
        '0832',
        '0833',
        '0838',
        '0859',
      ];

      if (AXISNEW.includes(prefix)) {
        return 'AXIS';
      }
    }

    if (TELKOMSEL.includes(prefix)) {
      return 'TELKOMSEL';
    }

    if (BYU.includes(prefix)) {
      return 'BYU';
    }

    if (INDOSAT.includes(prefix)) {
      return 'INDOSAT';
    }

    if (XL.includes(prefix)) {
      return 'XL';
    }

    if (THREE.includes(prefix)) {
      return 'TRI';
    }

    if (AXIS.includes(prefix)) {
      return 'AXIS';
    }

    if (SMART.includes(prefix)) {
      return 'SMARTFREN';
    }
  }

  errorHandler(error: HttpErrorResponse) {
    return throwError( error || 'server error.' );
  }
}
