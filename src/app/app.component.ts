import {Component, ViewChild} from '@angular/core';
import {IonicModule, ModalController, Platform, IonRouterOutlet} from '@ionic/angular';
import {PlatformLocation} from "@angular/common";
import {Router} from "@angular/router";
import {FcmService} from "./services/fcm/fcm.service";
import { SplashScreen } from '@capacitor/splash-screen';
import {AlertService} from "./services/alert/alert.service";

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [IonicModule],
})
export class AppComponent {
  @ViewChild(IonRouterOutlet, { static: false }) routerOutlet: IonRouterOutlet | undefined

  constructor(
    private platform: Platform,
    private location: PlatformLocation,
    private modal: ModalController,
    private router: Router,
    private alert: AlertService,
    private fcmService: FcmService
  ) {
    this.initializeApp();

    this.location.onPopState(async () => {
      const modal = await this.modal.getTop();
      if (modal) {
        await modal.dismiss();
      }
    })
  }

  initializeApp() {
    this.platform.ready().then(async () => {
      await SplashScreen.hide();
      if (this.platform.is('capacitor')) {
        try {
          await this.fcmService.registerNotifications();
          await this.fcmService.addListeners();
        } catch (e) {
          alert(JSON.stringify(e))
          alert('register push notification not grander')
        }
      }

      this.platform.backButton.subscribeWithPriority(0, async () => {
        if (this.routerOutlet) {
          if (this.routerOutlet.canGoBack()) {
            await this.routerOutlet.pop();
          } else {
            if (this.router.url === '/tabs/home' || this.router.url === '/login') {
              await this.alert.presentAlertConfirmation(
                'Tutup APlikasi',
                'Apakah anda yakin ingin keluar dari aplikasi ?',
                true
              )
            } else {
              this.location.back();
            }
          }
        } else {
          this.location.back();
        }
      });
    });
  }
}
