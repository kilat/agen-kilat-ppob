import { Component } from '@angular/core';
import {AuthenticationService} from '../services/authentication/authentication.service';
import {Router} from '@angular/router';
import {UserService} from '../services/user/user.service';
import {UserDataModel} from '../models/users.model';
import {LoadingService} from '../services/loading/loading.service';
import {AlertService} from '../services/alert/alert.service';
import {format, parseISO} from 'date-fns';
import {TrxService} from "../services/trx/trx.service";
import {TrxDataModel} from "../models/trx.model";
import {id} from "date-fns/locale";
import {IonicModule, Platform} from "@ionic/angular";
import {CommonModule} from "@angular/common";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  standalone: true,
  imports: [IonicModule, CommonModule, FormsModule],
})
export class HomePage {

  slidesOptions = {
    slidesPerView: 1.5
  };
  user: UserDataModel = new UserDataModel();
  params: {
    fromDate: string;
    toDate: string;
  } = {
    fromDate: '',
    toDate: '',
  };
  trx: TrxDataModel[] = [];

  constructor(
    private authService: AuthenticationService,
    private router: Router,
    private userService: UserService,
    private loadingService: LoadingService,
    private alertService: AlertService,
    private rrxService: TrxService,
    private platform: Platform,
  ) {
    console.log('open app');
    // this.platform.backButton.subscribeWithPriority(10, () => {
    //   console.log('Handler was called!');
    // });
  }

  async ionViewWillEnter() {
    await this.loadingService.present();
    try {
      this.params.fromDate = format(new Date(), 'yyyy-MM-dd', { locale: id });
      this.params.toDate = format(new Date(), 'yyyy-MM-dd', { locale: id });
      const response = await this.userService.getUserData();
      this.user = await response.data;
      this.getTrxData(this.user);
      // await this.loadingService.dismiss();
      // console.log(response.data)
      // if (!this.user.token && this.platform.is('capacitor')) {
      if (this.platform.is('capacitor')) {
          await this.setTokenPushNoty();
      }
      // await this.getTrxData(this.user);
      await this.loadingService.dismiss();

    } catch (e: any) {
      await this.loadingService.dismiss();
      await this.alertService.presentAlert('Error', e?.error?.message);
      if (e?.error?.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  async setTokenPushNoty() {
    try {
      await this.userService.setUserToken(this.user._id);
      console.log('update token notify success')
    } catch (e: any) {
      alert('update token error');
      // await this.alertService.presentAlert('Error', e.error.message);
      console.log('update token error is : ' + e?.error?.message);
      if (e.error.message === 'Token expired') {
        await this.logout();
      }
    }
  }

  getTrxData(userParams: UserDataModel) {
    this.trx = [];
    this.rrxService.getTrxData(this.params, userParams)
      .then(res => {
        this.trx = res.data;

        if (this.trx.length > 0) {
          for (const val of this.trx) {
            if(val.type === '00') {
              val.product_name = 'Manual Deposit'
              val.category_name = 'Deposit'
            }
            val.created_at = format(
              parseISO(format(new Date(val.created_at || '2023-04-14'), 'yyyy-MM-dd HH:mm')),
              'MMM d, yyyy HH:mm'
            );
          }
        }
      })
      .catch(async e => {
        if (e?.error?.message === 'Token expired') {
          await this.logout();
        }
      });
  }

  async goToPayment(val: string) {
    if (val === 'pln') {
      await this.router.navigate(['/tabs/home/pln']);
    } else if (val === 'PDAM001' || val === 'BPJS001' || val === 'TLKM001') {
      await this.router.navigate(['/tabs/home/postpaid', { category: val }]);
    } else {
      await this.router.navigate(['/tabs/home/pulsa', { category: val }]);
    }
  }

  async goToDet(data: any) {
    await this.router.navigate(['/tabs/trx/trx-detail', { dataDetail: JSON.stringify(data) }]);
  }

  async goToPage(val: string) {
    await this.router.navigate(['/tabs/' + val]);
  }

  async doRefresh(event: any) {
    setTimeout(async () => {
      console.log('Async operation has ended');
      await this.ionViewWillEnter()
      event.target.complete();
    }, 1000);
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', {replaceUrl: true});
  }

}
