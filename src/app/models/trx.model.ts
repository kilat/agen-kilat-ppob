export class TrxDataModel {
  fee_merchant?: number = 0;
  fee_biller?: number = 0;
  fee_kilat?: number = 0;
  amount?: number = 0;
  admin?: number = 0;
  total_amount?: number = 0;
  deducted_balance?: number = 0;
  type?: string = '';
  sign?: string = '';
  first_balance?: number = 0;
  last_balance?: number = 0;
  batch_time?: number = 0;
  sn: string = '';
  add_info1?: any = '';
  add_info2?: any = '';
  add_info3?: any = '';
  active?: number = 1;
  updated_at?: string = '';
  updated_by?: string = '';
  _id?: string = '';
  tx_id?: string = '';
  ref_id?: string = '';
  bill_id?: string = '';
  status?: string = '';
  merchant_code?: string = '';
  merchant_name?: string = '';
  bank_code?: string = '';
  bank_name?: string = '';
  category_code?: string = '';
  category_name?: string = '';
  product_code?: string = '';
  product_name?: string = '';
  req_param?: TrxDatareqParamModel = new TrxDatareqParamModel();
  res_param?: TrxDataresParamModel = new TrxDataresParamModel();
  created_at?: string = '';
  created_by?: string = '';
  tx_date?: string = '';
}

export class TrxDatareqParamModel {
  appName?: string = '';
  bill_id?: string = '';
  merchantToken?: string = '';
  product_code?: string = '';
  ref_id?: string = '';
  timeStamp?: string = '';
  user_id?: string = '';
}

export class TrxDataresParamModel {
  result_cd?: string = '';
  result_msg?: string = '';
  tx_id?: string = '';
  timeStamp?: string = '';
  user_id?: string = '';
  ref_id?: string = '';
  bill_id?: string = '';
  appName?: string = '';
  product_code?: string = '';
  merchantToken?: string = '';
  amount?: number;
  admin?: number;
  total_amount?: number;
  detail?: TrxDataresParamModelDetail = new TrxDataresParamModelDetail;
}

export class TrxDataresParamModelDetail {
  bill_id?: string = '';
  cust_name?: string = '';
  product_code?: string = '';
  kwh?: string = '';
  product_name?: string = '';
  ref_provider?: string = '';
  amount_provider?: number;
  admin_provider?: number;
  total_amount_provider?: number;
  sn?: string = '';
}

export class InqReqModel {
  timeStamp: string = '';
  user_id: string = '';
  ref_id: string = '';
  bill_id: string = '';
  appName: string = '';
  product_code: string = '';
  merchantToken: string = '';
  amount?: number;
}

export class InqResPulsaModel {
  result_cd: string = '';
  result_msg: string = '';
  tx_id: string = '';
  timeStamp: string = '';
  user_id: string = '';
  ref_id: string = '';
  bill_id: string = '';
  appName: string = '';
  product_code: string = '';
  merchantToken: string = '';
  amount: number = 0;
  admin: number = 0;
  total_amount: number = 0;
  detail: {
    product_name: string,
    sn: string,
    cust_name?: string;
    ref_provider?: string;
    amount_provider?: string;
    admin_provider?: string;
    total_amount_provider?: string;
    period?: string;
  } = {
    product_name: '',
    sn: '',
    cust_name: '',
    ref_provider: '',
    amount_provider: '',
    admin_provider: '',
    total_amount_provider: '',
    period: '',
  }
}


export class PayReqModel {
  timeStamp: string = '';
  user_id: string = '';
  ref_id: string = '';
  bill_id: string = '';
  appName: string = '';
  product_code: string = '';
  merchantToken: string = '';
  tx_id: string = '';
  amount?: number = 0;
  admin?: number = 0;
  total_amount?: number = 0;
}

export class PayResPulsaModel {
  result_cd?: string = '';
  result_msg?: string = '';
  tx_id?: string = '';
  timeStamp?: string = '';
  bill_id?: string = '';
  product_code?: string = '';
  appName?: string = '';
  ref_id?: string = '';
  merchantToken?: string = '';
  amount?: number = 0;
  admin?: number = 0;
  total_amount?: number = 0;
  detail?: {
    product_name?: string,
    sn?: string,
  } = {
    product_name: '',
    sn: '',
  }
}
