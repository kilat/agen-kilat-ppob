export class ProductsModel {
  active?: number = 0;
  updated_at?: string = '2022-05-05T23:56:15.000Z';
  updated_by?: ProductsUsersModel = new ProductsUsersModel();
  _id?: string = '';
  code?: string = '';
  category?: ProductsCategoryModel = new ProductsCategoryModel();
  name?: string = '';
  price?: number = 0;
  provider_prod_code?: ProductsProvider = new ProductsProvider();
  created_by?: ProductsUsersModel = new ProductsUsersModel();
  created_at?: string = '';
  name_cat?: string = '';
  priceNum?: number = 0;
  brand?: string = '';
}

export class ProductsUsersModel {
  current_balance?: number = 0;
  _id?: string = '';
  full_name?: string = '';
  email?: string = '';
  phone_number?: string = '';
  identity_id?: string = '';
  role?: {
    code: string,
    level: number,
    name: string,
  };
  bank_code?: string = '';
  created_at?: string = '';
  created_by?: string = '';
}

export class ProductsCategoryModel {
  active?: number = 0;
  updated_at?: string = '';
  updated_by?: string = '';
  _id?: string = '';
  code?: string = '';
  name?: string = '';
  created_by?: string = '';
  created_at?: string = '';
}

export class ProductsProvider {
  password?: string = '';
  secret_code?: string = '';
  from_date?: string = '';
  active?: number = 1;
  updated_at?: string = '';
  updated_by?: ProductsUsersModel = new ProductsUsersModel();
  _id?: string = '';
  code?: string = '';
  name?: string = '';
  user?: string = '';
  to_date?: string = '';
  url?: {
    url?: string,
    method?: string,
    params?: string,
    url_inq?: string,
    url_pay?: string,
    url_rev?: string,
    url_adv?: string,
  };
  created_by?: ProductsUsersModel = new ProductsUsersModel();
  created_at?: string = '';
}
