export class UserDataModel {
  // eslint-disable-next-line @typescript-eslint/naming-convention
  current_balance?: number = 0;
  _id: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  full_name?: string = '';
  email?: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  phone_number?: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  identity_id?: string  = '';
  role: UserDataRoleModel = new UserDataRoleModel();
  // eslint-disable-next-line @typescript-eslint/naming-convention
  bank_code?: string = '';
  token?: string = '';
  password?: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  created_at?: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  created_by?: string = '';
  profile_pic?: string = '';

  old_password?: string = '';
  // password_new?: string = '';
  password_con?: string = '';
}

export class UserDataRoleModel {
  code?: string = '';
  level?: number = 0;
  name?: string = '';
}

export class RequestDepositModel {
  active?: number = 1;
  amount?: number = 0;
  img?: string = '';
  status?: string = '';
  // eslint-disable-next-line @typescript-eslint/naming-convention
  tx_id?: string = '';
}
