import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'ppob.ktm2.starter',
  appName: 'KTM PPOB 2',
  webDir: 'www',
  bundledWebRuntime: false,
  "plugins": {
    "PushNotifications": {
      "presentationOptions": ["badge", "sound", "alert"]
    },
    "GoogleAuth": {
      "scopes": ["profile", "email"],
      "serverClientId": "974935731868-jsk2ifn4rccv5lrk47vfog59jsaj877s.apps.googleusercontent.com",
      "forceCodeForRefreshToken": true
    }
  }
};

export default config;
